#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <errno.h>
#include <stdint.h>

#include ".\Crypto.h"

/* Read a line from stream*/
ssize_t getline(char **lineptr, size_t *n, FILE *stream) {
    size_t pos;
    int c;

    if (lineptr == NULL || stream == NULL || n == NULL) {
        errno = EINVAL;
        return -1;
    }

    c = fgetc(stream);
    if (c == EOF) {
        return -1;
    }

    if (*lineptr == NULL) {
        *lineptr = malloc(128);
        if (*lineptr == NULL) {
            return -1;
        }
        *n = 128;
    }

    pos = 0;
    while(c != EOF) {
        if (pos + 1 >= *n) {
            size_t new_size = *n + (*n >> 2);
            if (new_size < 128) {
                new_size = 128;
            }
            char *new_ptr = realloc(*lineptr, new_size);
            if (new_ptr == NULL) {
                return -1;
            }
            *n = new_size;
            *lineptr = new_ptr;
        }

        ((unsigned char *)(*lineptr))[pos ++] = c;
        if (c == '\n') {
            break;
        }
        c = fgetc(stream);
    }

    (*lineptr)[pos] = '\0';
    return pos;
}

/* Convert hex string to buffer */
unsigned int hex_string_input (char **result, FILE *stream)
{
    char *buffer = NULL;
    unsigned long long int n = 0;
    int read = getline(&buffer, &n, stream);
    char *pos = NULL;
    
    if (read != -1)
    {
        pos = buffer;
    }
    else
    {
        //printf("No line read...\n");
        return -1;
    }

    if (read % 2 != 0)
    {
        //printf("odd chars...\n");
        read--;// return -1;
    }
    
    char * temp_result = (char *) malloc(read/2);    

    for (int count = 0; count < (read/2); count++)
    {
        int temp;
        sscanf(pos, "%2x", &temp);
        temp_result[count] = temp;
        pos += 2;
    }
    
    *result = temp_result;
    free (buffer);
     
    return (read / 2);
}

/* Hex print buffer */
void print_hex(char * buffer, unsigned int n, int newline)
{   
    printf("0x");
    for(size_t count = 0; count < n; count++)
    {
      printf("%02x", buffer[count]);
    }
    
    if (newline)
    {
      puts("");
    }
}