/* Spares */

char replacement_keys[26] = {'e','t','a','o','i','n','s','h','r','d','l','c','u','m','w','f','g','y','p','b','v','k','j','x','q','z'};


int * alfabet_only_histogramn(char * s, int size)
{
  int * histogram = malloc (sizeof(int)*26);
  
  memset(histogram, 0, (sizeof(int)*26));
  
  for(int i = 0; i < size; i++)
  {
    char temp = tolower(s[i]);
    if((temp >= 'a') && (temp <= 'z'))
    {
      histogram[temp - 'a']++;
    }
  }
  
  return histogram;
}

int * alfabet_only_histogram(char * s)
{
  int * histogram = malloc (sizeof(int)*26);
  memset(histogram, 0, (sizeof(int)*26));
  
  for(int i = 0; s[i] != 0; i++)
  {
    char temp = tolower(s[i]);
    if((temp >= 'a') && (temp <= 'z'))
    {
      histogram[temp - 'a']++;
    }
  }
  
  return histogram;
}

char * substitution_crypt_x(char * data, int dn, int *hist)
{
  char * key = replacement_key(hist);
  char * crypt = char_substitute(key, data, dn);  
  return crypt;
}


char * replacement_key(int * hist)
{
  int * histogram = malloc(26 * sizeof(int));
  memcpy(histogram, hist, 26 * sizeof(int));
  char * key = malloc(26);
  
  for(int i = 0; i < 26; i++)
  {
    int best =0;
    int big = 0;
    
    for(int j = 0; j < 26; j++)
    {
      if (histogram[j] > best)
      {
        best = histogram[j];
        big = j;
      }        
    }
    key[i] = english_freq[big];
    histogram[big] = 0;
  }
  return key;
}

int * histogram(char * s)
{
  int * histogram = malloc (sizeof(int)*256);
  
  for (int i = 0; s[i] != '\0'; ++i)    // generate histogram for string s
  {
    histogram[(unsigned int)s[i]]++;
  }
  return histogram;
}
