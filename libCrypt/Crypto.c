#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <errno.h>
#include <stdint.h>
#include <gmodule.h>

#include ".\Crypto.h"


/* Space, A-Z, Others*/
#define ENGLISH_FREQ_SIZE (28)
double english_freq[ENGLISH_FREQ_SIZE] = {0.14, 0.08, 0.01, 0.02, 0.04, 0.12, 0.02, 0.02, 0.06, 0.06, 0.01, 0.01, 0.04, 0.02, 0.06, 0.07, 0.01, 0.00, 0.05, 0.06, 0.09, 0.02, 0.01, 0.02, 0.00, 0.01, 0.00, 0.09};

float getChi2 (char * str, unsigned int length)
 {
    
    int count[ENGLISH_FREQ_SIZE] = {0};
    int ignored = 0;
    
    for (int i = 0; i < ENGLISH_FREQ_SIZE; i++) count[i] = 0;

    for (unsigned int i = 0; ((str[i] != 0)&&(i < length)); i++)
    {
        char c = str[i];
        if (c >= 65 && c <= 90) count[c - 65 +1]++;        // uppercase A-Z
        else if (c >= 97 && c <= 122) count[c - 97+1]++;  // lowercase a-z
        else if (c == 32) count[0]++;        // numbers and punct.
        else if (c > 32 && c <= 126) count[27]++;        // numbers and punct.
        else if (c == 9 || c == 10 || c == 13) ignored++;  // TAB, CR, LF
        else ignored++;  // not printable ASCII = impossible(?)
    }

    float chi2 = 0;
    int len = strlen(str) - ignored;
    for (int i = 0; i < ENGLISH_FREQ_SIZE; i++)
    {
        int observed = count[i];
        float expected = len * english_freq[i];
        int difference = observed - expected;
        chi2 += difference*difference / expected;
    }
    return chi2;
}

int is_alfa (char c)
{
  if (c >= 65 && c <= 90) return 1;        // uppercase A-Z
  else if (c >= 97 && c <= 122) return 1;  // lowercase a-z
  else if (c >= 32 && c <= 126) return 1;        // space, numbers and punct.
  else if (c == 9 || c == 10 || c == 13) return 0;  // TAB, CR, LF

  return 0;
}

/* Long Key substitution. Key and data match length */
char * xor_crypt_matched_key(char * data, unsigned int dn, char * key)
{
  char * crypt = malloc(dn);
  
  for(unsigned int count = 0; count < dn; count++)
  {
    crypt[count] = data[count] ^ key[count];
  }
  
  return crypt;
}

/* Long Key substitution. Key and data match length */
char * xor_crypt_repeat(char * data, unsigned int dn, char * key, unsigned int kn)
{
  char * crypt = malloc(dn);
  unsigned int kcount = 0;
  
  for(unsigned int count = 0; count < dn; count++)
  {
    crypt[count] = data[count] ^ key[kcount];
    kcount =  (kcount < (kn-1)) ? (kcount + 1) : (0);
  }
  
  return crypt;
}


char * char_substitute(char * key, char * data, int dn)
{
  char * crypt = malloc(dn);
  
  for(int count = 0; count < dn; count++)
  {
    crypt[count] = key[tolower(data[count]) - 'a'];
  }
  
  return crypt;  
}

unsigned char * byte_xor(unsigned int key, unsigned char * data, unsigned int dn)
{
	unsigned char * crypt = malloc(dn+1);

	for(unsigned int count = 0; count < dn; count++)
	{
	   crypt[count] = key^data[count];
	}

	return crypt;
}

void null_terminate(char * data, int dn)
{
	data[dn] = 0;
}

unsigned int best_xor(unsigned char * data, unsigned int dn, unsigned int character)
{
	unsigned int best_key = 0;
	int best_count = 0;

	for(int i = 0x0; i <= 0xff; i++)
	{
		int count = 0;
		for (unsigned int j = 0; j < dn; j++)
		{
			if(((unsigned int)(data[j] ^ i)) == character)
			{
				count++;
			}
		}

		if (count > best_count)
		{
			best_key = i;
			best_count = count;
		}
	}

	return best_key;
}

unsigned int best_xor_chi2(char * data, int dn)
{
	unsigned int best_key = 0.0;
	float best_score = 0;

	for(int i = 0x0; i <= 0xff; i++)
	{
    char * decrypt = (char *)byte_xor(i, (unsigned char *)data, dn);
    float score = getChi2(decrypt, dn);
    
		if (score > best_score)
		{
			best_key = i;
			best_score = score;
		}
    
    free (decrypt);
    decrypt = NULL;
	}

	return best_key;
}

/* TODO: combine */
unsigned int count_etaoi(char * data, int dn)
{
	int score = 0;

	for(int i = 0x0; i < dn; i++)
	{
    char c = data[i];
    int points = 0;

    switch (tolower(c)) {
       case 'e': ++points; break;
       case 't': ++points; break;
       case 'a': ++points; break;
       case 'o': ++points; break;
       case 'i': ++points; break;
       case ' ': ++points; break;
       case 's': ++points; break;
       case 'h': ++points; break;
       case 'r': ++points; break;
       case 'd': ++points; break;
       case 'l': ++points; break;
       case 'u': ++points; break;
    }
  }
  return score;
}

/* TODO: combine */
unsigned int best_xor_etaoi(char * data, int dn)
{
	unsigned int best_key = 0;
	int best_count = 0;

	for(int i = 0x0; i <= 0xff; i++)
	{
    int points = 0;
    for (int j = 0; j < dn; j++)
		{
      char c = data[j] ^ i;

      switch (tolower(c)) {
        case 'e': ++points; break;
        case 't': ++points; break;
        case 'a': ++points; break;
        case 'o': ++points; break;
        case 'i': ++points; break;
        case ' ': ++points; break;
        case 's': ++points; break;
        case 'h': ++points; break;
        case 'r': ++points; break;
        case 'd': ++points; break;
        case 'l': ++points; break;
        case 'u': ++points; break;
          break;
      }
		}

		if (points > best_count)
		{
			best_key = i;
			best_count = points;
		}
	}

	return best_key;
}

/* TODO: combine */
unsigned int best_xor_etaoi_alfa_key(unsigned char * data, int dn)
{
	unsigned int best_key = 0;
	int best_count = 0;

	for(int i = 32; i < 127; i++)
	{
		int points = 0;
		for (int j = 0; j < dn; j++)
		{
			char c = data[j] ^ i;

			switch (tolower(c)) {
				case 'e': ++points; break;
				case 't': ++points; break;
				case 'a': ++points; break;
				case 'o': ++points; break;
				case 'i': ++points; break;
				case ' ': ++points; break;
				case 's': ++points; break;
				case 'h': ++points; break;
				case 'r': ++points; break;
				case 'd': ++points; break;
				case 'l': ++points; break;
				case 'u': ++points; break;
			}
		}

		if (points > best_count)
		{
			best_key = i;
			best_count = points;
		}
	}

	return best_key;
}

float hamdist_keyscore(const unsigned char *txtdat, unsigned int txtsize, unsigned int keysize)
{
	unsigned int keyblk_idx, score = 0;
	unsigned char *curblk, *prevblk;

	curblk = malloc(keysize+1);
	if( NULL == curblk)
		return -1;
	curblk[keysize] = 0;

	prevblk = malloc(keysize+1);
	if( NULL == prevblk)
		return -1;
	prevblk[keysize] = 0;

	memcpy(curblk, txtdat, keysize);
	for (keyblk_idx = keysize; keyblk_idx < txtsize; keyblk_idx+=keysize)
	{

		memcpy(prevblk, curblk, keysize);
		memcpy(curblk, txtdat + keysize, keysize);
		score += hamming_distance(prevblk, curblk, keysize);
	}

	free(curblk);
	free(prevblk);

	return score/((float) keysize);
}

unsigned int find_keysize(unsigned char* crypt, unsigned int len, unsigned int minkeysize, unsigned int maxkeysize)
{
	unsigned int best_key;
	float best_key_score = 0.0;

	for (unsigned int i = minkeysize; i <= maxkeysize; i++)
	{
		float score = hamdist_keyscore(crypt, len, i);
		if(score > best_key_score)
		{
			best_key = i;
			best_key_score = score;
		}
	}
	return best_key;
}

typedef struct{
  unsigned int key;
	float score;
}key_candidate;

gint key_sort(gconstpointer a, gconstpointer b)
{
  if (((key_candidate *)a)->score > ((key_candidate *)(b))->score) return 1;
  else if (((key_candidate *)a)->score < ((key_candidate *)(b))->score) return -1;
  /* if (a.score == b.score)*/ return 0;  
  
}

GArray * score_keysizes(unsigned char* crypt, unsigned int len, unsigned int minkeysize, unsigned int maxkeysize)
{
  
  GArray * keys = g_array_new (FALSE, FALSE, sizeof(key_candidate));

	for (unsigned int i = minkeysize; i <= maxkeysize; i++)
	{
    key_candidate temp;
    temp.score = hamdist_keyscore(crypt, len, i);
    temp.key = i;
    g_array_append_val(keys,temp);
	}
  
  g_array_sort(keys,key_sort);
  
	return keys;
}

unsigned int count_char(char * data, int dn, unsigned int character)
{
	int count = 0;

	for(int i = 0x0; i < dn; i++)
	{
      if ((unsigned int)data[i] == character)
      {
        count++;
      }
	}

	return count;
}

size_t hamming_distance(const unsigned char* a, const unsigned char* b, size_t len)
{
   size_t dist = 0;
   size_t i = 0;
   for (i = 0; i < len; ++i) {
      unsigned char c = a[i] ^ b[i];
      while (c) {
         if (c & 0x01) { ++dist; }
         c = (c & 0xff) >> 1;
      }
   }
   return dist;
}
