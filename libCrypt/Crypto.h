#include "base64.h"

typedef intptr_t ssize_t;

float getChi2 (char * str, unsigned int count);
int * histogram(char * s);
char * xor_crypt_matched_key(char * data, unsigned int dn, char * key);
char * replacement_key(int * hist);
char * char_substitute(char * key, char * data, int dn);
char * substitution_crypt_x(char * data, int dn, int *hist);
unsigned int best_xor(unsigned char * data, unsigned int dn, unsigned int character);
unsigned char * byte_xor(unsigned int key, unsigned char * data, unsigned int dn);
void null_terminate(char * data, int dn);
unsigned int count_char(char * data, int dn, unsigned int character);
unsigned int best_xor_etaoi(char * data, int dn);
unsigned int best_xor_etaoi_alfa_key(unsigned char * data, int dn);
unsigned int best_xor_chi2(char * data, int dn);

char * xor_crypt_repeat(char * data, unsigned int dn, char * key, unsigned int kn);
size_t hamming_distance(const unsigned char* a, const unsigned char* b, size_t len);
float hamdist_keyscore(const unsigned char *txtdat, unsigned int txtsize, unsigned int keysize);
unsigned int find_keysize(unsigned char* crypt, unsigned int len, unsigned int minkeysize, unsigned int maxkeysize);

/* io.c */
ssize_t getline(char **lineptr, size_t *n, FILE *stream);
unsigned int hex_string_input (char **result, FILE *stream);
void print_hex(char * buffer, unsigned int n, int newline);
