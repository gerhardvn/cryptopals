#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <stdint.h>
#include <assert.h> 
#include <string.h> 

int main(int argc, char **argv) 
{
  FILE * f = fopen ("test_getline.txt", "rt");
  if (!f)
    {
      fputs ("Failed to open sample file.\n", stderr);
      remove ("test-getline.txt");
      return -1;
  }

  /* Test initial allocation, which must include trailing NUL.  */
  char * line = NULL;
  unsigned long int len = 0;
  
  int result = getline (&line, &len, f);
  assert (result == 2);
  assert (strcmp (line, "a\n") == 0);
  assert (2 < len);
  free (line);
  
  return 0;
}
