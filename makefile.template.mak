SHELL=cmd

all : $(number).exe
 
$(number).exe : $(number).o 
	gcc -o $(number).exe $(number).o -L../../libcrypt -lcrypto

$(number).o : $(number).c ../../libcrypt/libcrypto.a
	gcc -c -g -Wall -Wextra -o $(number).o $(number).c

clean:	
	rm -f *.o
	rm -f *.gc*
	rm -f *.stackdump
	rm -f *.exe
	rm -f tags.
	
test: $(number).exe input.txt
	$(number).exe input.txt > output.txt
	cmp output.txt expected.txt