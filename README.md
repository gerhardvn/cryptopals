# About CryptoPals

[The cryptopals crypto challenges](https://cryptopals.com/) 

- [Set 1: Basics](https://cryptopals.com/sets/1)
- [Set 2: Block crypto](https://cryptopals.com/sets/2)
- [Set 3: Block & stream crypto](https://cryptopals.com/sets/3)
- [Set 4: Stream crypto and randomness](https://cryptopals.com/sets/4)
- [Set 5: Diffie-Hellman and friends](https://cryptopals.com/sets/5)
- [Set 6: RSA and DSA](https://cryptopals.com/sets/6)
- [Set 7: Hashes](https://cryptopals.com/sets/7)
- [Set 8: Abstract Algebra](https://cryptopals.com/sets/8)

# About this

This is my solutions in C.
Every thing has a makefile.
There is project file for [Eclipse CDT](https://www.eclipse.org/cdt/).

## Lib Cripto
Contains code reused between solutions.