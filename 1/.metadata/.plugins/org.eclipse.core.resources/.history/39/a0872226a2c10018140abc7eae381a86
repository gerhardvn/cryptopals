#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <errno.h>
#include <stdint.h>
#include ".\Crypto.h"

// http://en.algoritmy.net/article/40379/Letter-frequency-English
double english_freq[26] = {
    0.08167, 0.01492, 0.02782, 0.04253, 0.12702, 0.02228, 0.02015,  // A-G
    0.06094, 0.06966, 0.00153, 0.00772, 0.04025, 0.02406, 0.06749,  // H-N
    0.07507, 0.01929, 0.00095, 0.05987, 0.06327, 0.09056, 0.02758,  // O-U
    0.00978, 0.02360, 0.00150, 0.01974, 0.00074                     // V-Z
};

char replacement_keys[26] = {'e','t','a','o','i','n','s','h','r','d','l','c','u','m','w','f','g','y','p','b','v','k','j','x','q','z'};

double getChi2 (char * str)
 {
    int count[26] = {0};
    int ignored = 0;
    
    for (int i = 0; i < 26; i++) count[i] = 0;

    for (int i = 0; str[i] != 0; i++)
    {
        char c = str[i];
        if (c >= 65 && c <= 90) count[c - 65]++;        // uppercase A-Z
        else if (c >= 97 && c <= 122) count[c - 97]++;  // lowercase a-z
        else if (c >= 32 && c <= 126) ignored++;        // numbers and punct.
        else if (c == 9 || c == 10 || c == 13) ignored++;  // TAB, CR, LF
        else ignored++;  // not printable ASCII = impossible(?)
    }

    double chi2 = 0;
    int len = strlen(str) - ignored;
    for (int i = 0; i < 26; i++)
    {
        int observed = count[i];
        double expected = len * english_freq[i];
        int difference = observed - expected;
        chi2 += difference*difference / expected;
    }
    return chi2;
}

int is_alfa (char c)
{
  if (c >= 65 && c <= 90) return 1;        // uppercase A-Z
  else if (c >= 97 && c <= 122) return 1;  // lowercase a-z
  else if (c >= 32 && c <= 126) return 1;        // space, numbers and punct.
  else if (c == 9 || c == 10 || c == 13) return 0;  // TAB, CR, LF

  return 0;
}

int * histogram(char * s)
{
  int * histogram = malloc (sizeof(int)*256);
  
  for (int i = 0; s[i] != '\0'; ++i)    // generate histogram for string s
  {
    histogram[(unsigned int)s[i]]++;
  }
  return histogram;
}

int * alfabet_only_histogramn(char * s, int size)
{
  int * histogram = malloc (sizeof(int)*26);
  
  memset(histogram, 0, (sizeof(int)*26));
  
  for(int i = 0; i < size; i++)
  {
    char temp = tolower(s[i]);
    if((temp >= 'a') && (temp <= 'z'))
    {
      histogram[temp - 'a']++;
    }
  }
  
  return histogram;
}

int * alfabet_only_histogram(char * s)
{
  int * histogram = malloc (sizeof(int)*26);
  memset(histogram, 0, (sizeof(int)*26));
  
  for(int i = 0; s[i] != 0; i++)
  {
    char temp = tolower(s[i]);
    if((temp >= 'a') && (temp <= 'z'))
    {
      histogram[temp - 'a']++;
    }
  }
  
  return histogram;
}

typedef intptr_t ssize_t;

/* web.mit.edu/freebsd/head/contrib/wpa/src/utils/base64.c */

static const unsigned char base64_table[65] =
	"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

/**
 * base64_encode - Base64 encode
 * @src: Data to be encoded
 * @len: Length of the data to be encoded
 * @out_len: Pointer to output length variable, or %NULL if not used
 * Returns: Allocated buffer of out_len bytes of encoded data,
 * or %NULL on failure
 *
 * Caller is responsible for freeing the returned buffer. Returned buffer is
 * nul terminated to make it easier to use as a C string. The nul terminator is
 * not included in out_len.
 */
unsigned char * base64_encode(const unsigned char *src, size_t len, size_t *out_len)
{
	unsigned char *out, *pos;
	const unsigned char *end, *in;
	size_t olen;
	int line_len;

	olen = len * 4 / 3 + 4; /* 3-byte blocks to 4-byte */
	olen += olen / 72; /* line feeds */
	olen++; /* nul termination */
	if (olen < len)
		return NULL; /* integer overflow */
	out = malloc(olen);
	if (out == NULL)
		return NULL;

	end = src + len;
	in = src;
	pos = out;
	line_len = 0;
	while (end - in >= 3) {
		*pos++ = base64_table[in[0] >> 2];
		*pos++ = base64_table[((in[0] & 0x03) << 4) | (in[1] >> 4)];
		*pos++ = base64_table[((in[1] & 0x0f) << 2) | (in[2] >> 6)];
		*pos++ = base64_table[in[2] & 0x3f];
		in += 3;
		line_len += 4;
		if (line_len >= 72) {
			*pos++ = '\n';
			line_len = 0;
		}
	}

	if (end - in) {
		*pos++ = base64_table[in[0] >> 2];
		if (end - in == 1) {
			*pos++ = base64_table[(in[0] & 0x03) << 4];
			*pos++ = '=';
		} else {
			*pos++ = base64_table[((in[0] & 0x03) << 4) |
					      (in[1] >> 4)];
			*pos++ = base64_table[(in[1] & 0x0f) << 2];
		}
		*pos++ = '=';
		line_len += 4;
	}

	if (line_len)
		*pos++ = '\n';

	*pos = '\0';
	if (out_len)
		*out_len = pos - out;
	return out;
}

/**
 * base64_decode - Base64 decode
 * @src: Data to be decoded
 * @len: Length of the data to be decoded
 * @out_len: Pointer to output length variable
 * Returns: Allocated buffer of out_len bytes of decoded data,
 * or %NULL on failure
 *
 * Caller is responsible for freeing the returned buffer.
 */
unsigned char * base64_decode(const unsigned char *src, size_t len,
			      size_t *out_len)
{
	unsigned char dtable[256], *out, *pos, block[4], tmp;
	size_t i, count, olen;
	int pad = 0;

	memset(dtable, 0x80, 256);
	for (i = 0; i < sizeof(base64_table) - 1; i++)
		dtable[base64_table[i]] = (unsigned char) i;
	dtable['='] = 0;

	count = 0;
	for (i = 0; i < len; i++) {
		if (dtable[src[i]] != 0x80)
			count++;
	}

	if (count == 0 || count % 4)
		return NULL;

	olen = count / 4 * 3;
	pos = out = malloc(olen);
	if (out == NULL)
		return NULL;

	count = 0;
	for (i = 0; i < len; i++) {
		tmp = dtable[src[i]];
		if (tmp == 0x80)
			continue;

		if (src[i] == '=')
			pad++;
		block[count] = tmp;
		count++;
		if (count == 4) {
			*pos++ = (block[0] << 2) | (block[1] >> 4);
			*pos++ = (block[1] << 4) | (block[2] >> 2);
			*pos++ = (block[2] << 6) | block[3];
			count = 0;
			if (pad) {
				if (pad == 1)
					pos--;
				else if (pad == 2)
					pos -= 2;
				else {
					/* Invalid padding */
					free(out);
					return NULL;
				}
				break;
			}
		}
	}

	*out_len = pos - out;
	return out;
}

/* Read a line from stream*/
ssize_t getline(char **lineptr, size_t *n, FILE *stream) {
    size_t pos;
    int c;

    if (lineptr == NULL || stream == NULL || n == NULL) {
        errno = EINVAL;
        return -1;
    }

    c = fgetc(stream);
    if (c == EOF) {
        return -1;
    }

    if (*lineptr == NULL) {
        *lineptr = malloc(128);
        if (*lineptr == NULL) {
            return -1;
        }
        *n = 128;
    }

    pos = 0;
    while(c != EOF) {
        if (pos + 1 >= *n) {
            size_t new_size = *n + (*n >> 2);
            if (new_size < 128) {
                new_size = 128;
            }
            char *new_ptr = realloc(*lineptr, new_size);
            if (new_ptr == NULL) {
                return -1;
            }
            *n = new_size;
            *lineptr = new_ptr;
        }

        ((unsigned char *)(*lineptr))[pos ++] = c;
        if (c == '\n') {
            break;
        }
        c = fgetc(stream);
    }

    (*lineptr)[pos] = '\0';
    return pos;
}

/* Convert hex string to buffer */
ssize_t hex_string_input (char **result, size_t *n, FILE *stream)
{
    char *buffer = NULL;
    int read = getline(&buffer, n, stream);
    char *pos = NULL;
    
    if (read != -1)
    {
        pos = buffer;
    }
    else
    {
        //printf("No line read...\n");
        return -1;
    }

    if (read % 2 != 0)
    {
        //printf("odd chars...\n");
        read--;// return -1;
    }
    
    char * temp_result = (char *) malloc(read/2);    

    for (int count = 0; count < (read/2); count++)
    {
        int temp;
        sscanf(pos, "%2x", &temp);
        temp_result[count] = temp;
        pos += 2;
    }
    
    *result = temp_result;
    free (buffer);
     
    return (read / 2);
}

/* Hex print buffer */
void print_hex(char * buffer, unsigned int n, int newline)
{   
    printf("0x");
    for(size_t count = 0; count < n; count++)
    {
      printf("%02x", buffer[count]);
    }
    
    if (newline)
    {
      puts("");
    }
}

/* Long Key substitution. Key and data match length */
char * xor_crypt(char * data, unsigned int dn, char * key)
{
  char * crypt = malloc(dn);
  
  for(unsigned int count = 0; count < dn; count++)
  {
      crypt[count] = data[count] ^ key[count];
  }
  
  return crypt;
}

/* Long Key substitution. Key and data match length */
char * xor_crypt_repeat(char * data, unsigned int dn, char * key, unsigned int kn)
{
  char * crypt = malloc(dn);
  unsigned int kcount = 0;
  
  for(unsigned int count = 0; count < dn; count++)
  {
    crypt[count] = data[count] ^ key[kcount];
    kcount =  (kcount < (kn-1)) ? (kcount + 1) : (0);
  }
  
  return crypt;
}

char * replacement_key(int * hist)
{
  int * histogram = malloc(26 * sizeof(int));
  memcpy(histogram, hist, 26 * sizeof(int));
  char * key = malloc(26);
  
  for(int i = 0; i < 26; i++)
  {
    int best =0;
    int big = 0;
    
    for(int j = 0; j < 26; j++)
    {
      if (histogram[j] > best)
      {
        best = histogram[j];
        big = j;
      }        
    }
    key[i] = english_freq[big];
    histogram[big] = 0;
  }
  return key;
}


char * char_substitute(char * key, char * data, int dn)
{
  char * crypt = malloc(dn);
  
  for(int count = 0; count < dn; count++)
  {
     crypt[count] = key[tolower(data[count]) - 'a'];
  }
  
  return crypt;  
}

char * byte_xor(unsigned int key, unsigned char * data, unsigned int dn)
{
	  char * crypt = malloc(dn+1);

	  for(unsigned int count = 0; count < dn; count++)
	  {
	     crypt[count] = key^data[count];
	  }

	  return crypt;
}

void null_terminate(char * data, int dn)
{
	data[dn] = 0;
}

char * substitution_crypt_x(char * data, int dn, int *hist)
{
  char * key = replacement_key(hist);
  char * crypt = char_substitute(key, data, dn);  
  return crypt;
}

unsigned int best_xor(unsigned char * data, int dn, unsigned int character)
{
	unsigned int best_key = 0;
	int best_count = 0;

	for(int i = 0x0; i <= 0xff; i++)
	{
		int count = 0;
		for (int j = 0; j < dn; j++)
		{
			if(((unsigned int)(data[j] ^ i)) == character)
			{
				count++;
			}
		}

		if (count > best_count)
		{
			best_key = i;
			best_count = count;
		}
	}

	return best_key;
}

unsigned int count_etaoi(char * data, int dn)
{
	int score = 0;

	for(int i = 0x0; i < dn; i++)
	{
    char c = data[i];
    int points = 0;

    switch (tolower(c)) {
       case 'e': ++points;
       case 't': ++points;
       case 'a': ++points;
       case 'o': ++points;
       case 'i': ++points;
       case ' ': ++points;
       case 's': ++points;
       case 'h': ++points;
       case 'r': ++points;
       case 'd': ++points;
       case 'l': ++points;
       case 'u': ++points;
          break;
    }
  }
  return score;
}

unsigned int best_xor_etaoi(char * data, int dn)
{
	unsigned int best_key = 0;
	int best_count = 0;

	for(int i = 0x0; i <= 0xff; i++)
	{
    int points = 0;
    for (int j = 0; j < dn; j++)
		{
      char c = data[j] ^ i;

      switch (tolower(c)) {
        case 'e': ++points;
        case 't': ++points;
        case 'a': ++points;
        case 'o': ++points;
        case 'i': ++points;
        case ' ': ++points;
        case 's': ++points;
        case 'h': ++points;
        case 'r': ++points;
        case 'd': ++points;
        case 'l': ++points;
        case 'u': ++points;
          break;
      }
		}

		if (points > best_count)
		{
			best_key = i;
			best_count = points;
		}
	}

	return best_key;
}

unsigned int best_xor_etaoi_alfa_key(unsigned char * data, int dn)
{
	unsigned int best_key = 0;
	int best_count = 0;

	for(int i = 32; i < 127; i++)
	{
		int points = 0;
		for (int j = 0; j < dn; j++)
		{
			char c = data[j] ^ i;

			switch (tolower(c)) {
				case 'e': ++points;
				case 't': ++points;
				case 'a': ++points;
				case 'o': ++points;
				case 'i': ++points;
				case ' ': ++points;
				case 's': ++points;
				case 'h': ++points;
				case 'r': ++points;
				case 'd': ++points;
				case 'l': ++points;
				case 'u': ++points;
				break;
			}
		}

		if (points > best_count)
		{
			best_key = i;
			best_count = points;
		}
	}

	return best_key;
}

float hamdist_keyscore(const unsigned char *txtdat, unsigned int txtsize, unsigned int keysize)
{
	unsigned int keyblk_idx, score = 0;
	unsigned char *curblk, *prevblk;

	curblk = malloc(keysize+1);
	if( NULL == curblk)
		return -1;
	curblk[keysize] = 0;

	prevblk = malloc(keysize+1);
	if( NULL == prevblk)
		return -1;
	prevblk[keysize] = 0;

	memcpy(curblk, txtdat, keysize);
	for (keyblk_idx = keysize; keyblk_idx < txtsize; keyblk_idx+=keysize)
	{

		memcpy(prevblk, curblk, keysize);
		memcpy(curblk, txtdat + keysize, keysize);
		score += hamming_distance(prevblk, curblk, keysize);
	}

	free(curblk);
	free(prevblk);

	return score/((float) keysize);
}

unsigned int find_keysize(unsigned char* crypt, unsigned int len, unsigned int minkeysize, unsigned int maxkeysize)
{
	unsigned int best_key;
	float best_key_score = 0.0;

	for (unsigned int i = minkeysize; i <= maxkeysize; i++)
	{
		float score = hamdist_keyscore(crypt, len, i);
		if(score > best_key_score)
		{
			best_key = i;
			best_key_score = score;
		}
	}
	return best_key;
}


unsigned int count_char(char * data, int dn, unsigned int character)
{
	int count = 0;

	for(int i = 0x0; i < dn; i++)
	{
      if ((unsigned int)data[i] == character)
      {
        count++;
      }
	}

	return count;
}

size_t hamming_distance(const unsigned char* a, const unsigned char* b, size_t len)
{
   size_t dist = 0;
   size_t i = 0;
   for (i = 0; i < len; ++i) {
      unsigned char c = a[i] ^ b[i];
      while (c) {
         if (c & 0x01) { ++dist; }
         c = (c & 0xff) >> 1;
      }
   }
   return dist;
}
