#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <stdint.h>
#include "..\..\libCrypt\crypto.h"

int main(int argc, char **argv) 
{
    char *buffer = NULL;
    int read = 0;
    unsigned long long int converted = 0;
    FILE* fh;

    if (argc == 1)
    {
      read = hex_string_input(&buffer, stdin);
    }
    else
    {
       fh = fopen(argv[1], "rt"); 
       read = hex_string_input(&buffer, fh);
    }
    
    print_hex(buffer, read,1);

    unsigned char * base64 = base64_encode((unsigned char *)buffer, read, &converted);
    
    printf("%s",(char *)base64);
    
    free (buffer);
    free (base64);
    
    return 0;
}
