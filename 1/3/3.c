#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <errno.h>
#include <stdint.h>
#include "..\..\libCrypt\crypto.h"
#include <inttypes.h>

int main(int argc, char **argv)
{
    char *data = NULL;
    unsigned int cread = 0;
    FILE* fh;

    if (argc == 1)
    {
      cread = hex_string_input(&data, stdin);
    }
    else
    {
      fh = fopen(argv[1], "rt"); 
      cread = hex_string_input(&data, fh);
    }
    
    unsigned int key = best_xor((unsigned char *)data, cread, (unsigned int)' ');
    char * crypt = (char *) byte_xor(key, (unsigned char *)data, cread);
    
    print_hex(data, cread, 1);
    printf("Key = 0x%02x Bytes=%u\n",key,cread);
    print_hex(crypt, cread, 1);
    null_terminate(crypt, cread);
    printf("%s  %I64u",crypt, strlen(crypt));
  
    free(data);
    free(crypt);
    
    return 0;
}

