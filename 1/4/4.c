#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <errno.h>
#include <stdint.h>
#include "..\..\libCrypt\crypto.h"

int main(int argc, char **argv) 
{
  char *data = NULL;
  char * best = NULL;
  double best_score = 0.0;
  int read = 0;

  FILE* fh;

  if (argc == 1)
  {
    printf("Useage: ...");
    return -1;
  }

  fh = fopen(argv[1], "rt"); 
  read = hex_string_input(&data, fh);

  while (read > 0)
  {    
    unsigned int key = best_xor_etaoi(data, read);
    char * crypt = (char *) byte_xor( key,(unsigned char *)data, read);
    int score = count_char(crypt, read, (unsigned int)' ');
    null_terminate(crypt, read);
    
    if (score > best_score)
    {
      free (best);
      best = crypt;
      best_score = score;
    }
    else
    {
      free (crypt);
    }
    
    free (data);
    read = hex_string_input(&data, fh);
  }

  printf("Best : %s : %f" , best ,best_score);

  free(best);
  return 0;
}


