#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <errno.h>
#include <stdint.h>
#include "..\..\libCrypt\crypto.h"

int main(int argc, char **argv) 
{
  char data[] = "Burning 'em, if you ain't quick and nimble\nI go crazy when I hear a cymbal";
  char key[] = "ICE";

  int datan = strlen(data);
  char * crypt = xor_crypt_repeat(data,datan, key, 3);
  print_hex(crypt, datan, 1);
  free(crypt);    
  
  return 0;
}


