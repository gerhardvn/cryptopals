all : 5.exe
 
5.exe : 5.o 
	gcc -o 5.exe 5.o -L../../libcrypt -lcrypto

5.o : 5.c 
	gcc -c -g -Wall -Wextra -o 5.o 5.c

clean:	
	rm -f *.o
	rm -f *.gc*
	rm -f *.stackdump
	rm -f *.exe
	rm -f tags.
	