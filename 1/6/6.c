#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <errno.h>
#include <stdint.h>
#include "..\..\libCrypt\crypto.h"

char * read_block(FILE* fh, int * read_total)
{
  char *data = NULL;
  char *buffer = NULL;
  unsigned long long int len = 0;
      
  int read = getline(&buffer, &len, fh);
  int size = 0;
  
  while(read > 1)
  {
    //printf("%d .. %d\n", read, size);
	  read -= 1; // remove newline
    data = realloc(data, size + read);
    if(NULL == data){
      exit(-11);
    }
    
    memcpy(&data[size],buffer,read);
    size += read;
    free(buffer);
    buffer = NULL;
    len = 0;
    read = getline(&buffer, &len, fh);
  }
  *read_total = size;
  return data;
}

unsigned char * skip_copy_down(unsigned char * in, int size, int skip) 
{
    unsigned char * out = malloc((size/skip)+1);
    if(NULL == out){
      exit(-11);
    }
    
    for(int i = 0; i < (size/skip); i++)
    {
      out[i] = in[i*skip];
    }
    return out;
}

void skip_copy_up(unsigned char * in, unsigned char * out, int size, int skip)
{
    if(NULL == out){
      exit(-11);
    }
    
    for(int i = 0; i < (size/skip); i++)
    {
      out[i*skip] = in[i];
    }
}

int main(int argc, char **argv) 
{
  char *data = NULL;
  
  int read = 0;
  unsigned int converted =0;

  FILE* fh;

  if (argc == 1)
  {
    fh = stdin;
  }
  else
  {
    fh = fopen(argv[1], "rt"); 
  }

  data = read_block(fh, & read);
  
  unsigned char * bin = base64_encode((unsigned char *)data, read, &converted);
  free(data);

  unsigned int key_size = find_keysize(bin, converted, 2, 40);
  printf("Key size: %d\nKey ", key_size);
  
  unsigned char * output = malloc(converted);
  char key_phrase[128] = "";

  for(unsigned int i = 0; i < key_size; i++)
  {
    unsigned char * out = skip_copy_down(&bin[i], converted, key_size);
    unsigned int key = best_xor_chi2(out, converted/key_size);
    unsigned char * crypt = byte_xor( key, out, converted/key_size);
    skip_copy_up(crypt, &output[i], converted, key_size);
    
    key_phrase[i] = key;
    key_phrase[i+1] = 0;
    printf("%02x ", key);

    free (out);
    free (crypt);
  }
  
  printf("\n%s\n", key_phrase);

  free(bin);
  free(output);

}

