#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <stdint.h>
#include "..\..\libCrypt\crypto.h"

int main(int argc, char **argv) 
{
    char *data = NULL;
    char *key = NULL;
    char *crypt = NULL;
    
    int readd = 0;
    int readk = 0;
   
    FILE* fh;

    if (argc == 1)
    {
      readd = hex_string_input(&data, stdin);
      readk = hex_string_input(&key, stdin);
    }
    else
    {
      fh = fopen(argv[1], "rt"); 
      readd = hex_string_input(&data, fh);
      readk = hex_string_input(&key, fh);
    }

    print_hex(data,readd,1);
    print_hex(key,readk,1);
    
    crypt = xor_crypt_matched_key(data, readd, key);
    
    print_hex(crypt,readd,1);
    
    free (data);
    free (key);
    free (crypt);
    
    return 0;
}
